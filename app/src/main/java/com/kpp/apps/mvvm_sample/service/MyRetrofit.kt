package com.kpp.apps.mvvm_sample.service

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory



object MyRetrofit {

    private var retrofit: Retrofit? = null
    private val BASE_URL = "https://jsonplaceholder.typicode.com"

    fun getInstance() : Retrofit {
        if (retrofit == null) {
            retrofit = retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit!!
    }

}