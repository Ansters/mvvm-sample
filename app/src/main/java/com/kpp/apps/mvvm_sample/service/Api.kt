package com.kpp.apps.mvvm_sample.service

import com.kpp.apps.mvvm_sample.ui.main.model.Post
import retrofit2.Call
import retrofit2.http.GET

interface Api {

    @GET("/posts")
    fun getPosts() : Call<List<Post>>

}