package com.kpp.apps.mvvm_sample.ui.main

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kpp.apps.mvvm_sample.service.Api
import com.kpp.apps.mvvm_sample.service.MyRetrofit
import com.kpp.apps.mvvm_sample.ui.main.model.Post
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {

    val postsData = MutableLiveData<List<Post>>()

    fun getAllPosts() {
        val retrofit = MyRetrofit.getInstance().create(Api::class.java)
        val call = retrofit.getPosts()
        call.enqueue(object : Callback<List<Post>> {
            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                Log.e("PostError", t.toString())
            }

            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                postsData.postValue(response.body())
            }

        })
    }

}