package com.kpp.apps.mvvm_sample.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.kpp.apps.mvvm_sample.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainVM: MainViewModel
    private lateinit var adapter: PostAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainVM = ViewModelProviders.of(this).get(MainViewModel::class.java)
        initList()
        observeData()

        mainVM.getAllPosts()
    }

    private fun initList() {
        adapter = PostAdapter()
        posts.adapter = adapter
        posts.layoutManager = LinearLayoutManager(this)
    }

    private fun observeData() {
        mainVM.postsData.observe(this, Observer { post ->
            adapter.submitList(post)
        })
    }
}
