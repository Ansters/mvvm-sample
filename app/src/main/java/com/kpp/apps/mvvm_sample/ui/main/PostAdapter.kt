package com.kpp.apps.mvvm_sample.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.kpp.apps.mvvm_sample.R
import com.kpp.apps.mvvm_sample.ui.main.model.Post
import kotlinx.android.synthetic.main.item_post.view.*

class PostAdapter : ListAdapter<Post, PostAdapter.PostViewHolder>(
    DiffPost()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return PostViewHolder(inflater.inflate(R.layout.item_post, parent, false))
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.title.text = item.title
        holder.itemView.body.text = item.body
    }


    class PostViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }

    class DiffPost : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem.title == newItem.title && oldItem.body == newItem.body
        }

    }

}