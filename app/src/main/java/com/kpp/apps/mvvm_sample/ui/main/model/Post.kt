package com.kpp.apps.mvvm_sample.ui.main.model

import com.google.gson.annotations.SerializedName

data class Post(
    @SerializedName("userId") val userID: Int,
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("body") val body: String
)